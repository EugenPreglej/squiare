using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;
using Mapbox.Unity;
using Mapbox.Unity.Map;
using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;
using Mapbox.Utils.JsonConverters;
using Mapbox.Json;
using Mapbox.Directions;
using System.Linq;

public class POIManager : MonoBehaviour
{
    [SerializeField]
    private List<string> initial = new List<string>();
    private Dictionary<string, int> pointsOfInterest = new Dictionary<string, int>();
    private string poiFilePath = "POI.json";
    private string filePath;


    void Awake()
    {
        filePath = Application.persistentDataPath + "/" + poiFilePath;
    }


    void Start()
    {
        GetPOI();
    }


    private void ReadPointsOfInterest()
    {
        if (File.Exists(filePath))
        {
            string data = File.ReadAllText(filePath);
            pointsOfInterest = JsonConvert.DeserializeObject<Dictionary<string, int>>(data);
        }
    }


    private void WritePointsOfInterest()
    {
        var data = JsonConvert.SerializeObject(pointsOfInterest, Formatting.Indented, JsonConverters.Converters);
        File.WriteAllText(Application.persistentDataPath + "/" + poiFilePath, data);
    }


    public Dictionary<string, int> GetPOI()
    {
        if (File.Exists(filePath))
        {
            ReadPointsOfInterest();
        }

        else
        {
            foreach (string poi in initial)
            {
                pointsOfInterest[poi] = 0;
            }

            WritePointsOfInterest();
        }
        
        return pointsOfInterest;
    }

    private void AddPOI(string poi)
    {
        GetPOI();

        if (!pointsOfInterest.ContainsKey(poi))
        {
            pointsOfInterest.Add(poi, 0);
        }
        
        WritePointsOfInterest();
    }


    // newState = 1 znaci conquered
    public void EditPOI(string poi, int newState)
    {
        GetPOI();

        foreach (string key in pointsOfInterest.Keys)
        {
            if (key == poi)
            {
                pointsOfInterest[key] = newState;
            }
        }

        WritePointsOfInterest();
    }


    private void RemovePOI(string poi)
    {
        pointsOfInterest.Remove(poi);
        WritePointsOfInterest();
    }
}
