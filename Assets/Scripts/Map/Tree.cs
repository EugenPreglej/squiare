using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    void Start()
    {
        // Get a random rotation around the Y axis
        Quaternion randomRotation = Quaternion.Euler(0, Random.Range(0, 360), 0);

        // Set the GameObject's rotation to the random rotation
        transform.rotation = randomRotation;

        // Get a random scale between 0.7 and 1
        float randomScale = Random.Range(0.65f, 1f);

        // Set the GameObject's scale to the random scale
        transform.localScale = new Vector3(randomScale, randomScale, randomScale);
    }
}
