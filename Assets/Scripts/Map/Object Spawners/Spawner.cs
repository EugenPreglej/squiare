using UnityEngine;
using System.Collections.Generic;

public class Spawner : MonoBehaviour
{
    private int numChildren;
    public GameObject grassPrefab;
    public GameObject rockPrefab;
    public GameObject stumpPrefab;
    public GameObject catapultPrefab;
    public HashSet<Transform> _processedTiles = new HashSet<Transform>();

    private void Start()
    {
        numChildren = transform.childCount;
    }

    private void FixedUpdate()
    {
        if (numChildren != transform.childCount)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform child = transform.GetChild(i);
                if (child.name != "TileProvider" && !_processedTiles.Contains(child))
                {
                    CatapultSpawner catapultSpawner = child.gameObject.AddComponent<CatapultSpawner>();
                    catapultSpawner.catapultPrefab = catapultPrefab;
                    catapultSpawner.Spawn();

                    GrassSpawner grassSpawner = child.gameObject.AddComponent<GrassSpawner>();
                    grassSpawner.grassPrefab = grassPrefab;
                    grassSpawner.Spawn();

                    RockSpawner rockSpawner = child.gameObject.AddComponent<RockSpawner>();
                    rockSpawner.rockPrefab = rockPrefab;
                    rockSpawner.Spawn();

                    StumpSpawner stumpSpawner = child.gameObject.AddComponent<StumpSpawner>();
                    stumpSpawner.stumpPrefab = stumpPrefab;
                    stumpSpawner.Spawn();

                    _processedTiles.Add(child);
                }
            }
            numChildren = transform.childCount;
        }
    }
}
