using UnityEngine;

public class CatapultSpawner : MonoBehaviour
{
    public GameObject catapultPrefab;
    private int numObj;

    public void Spawn()
    {
        numObj = Random.Range(1, 2);
        if (catapultPrefab == null) return;

        Bounds bounds = GetComponent<MeshFilter>().mesh.bounds;

        float minDistance = 5f;
        Vector3[] positions = new Vector3[numObj];
        int numSpawned = 0;

        while (numSpawned < numObj)
        {
            float x = Random.Range(bounds.min.x, bounds.max.x);
            float z = Random.Range(bounds.min.z, bounds.max.z);
            Vector3 position = new Vector3(x, 0, z);

            bool isTooClose = false;
            for (int i = 0; i < numSpawned; i++)
            {
                if (Vector3.Distance(position, positions[i]) < minDistance)
                {
                    isTooClose = true;
                    break;
                }
            }

            if (!isTooClose)
            {
                Quaternion rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
                GameObject spawned = Instantiate(catapultPrefab, transform.TransformPoint(position), rotation, transform);
                spawned.isStatic = true;
                positions[numSpawned] = position;
                numSpawned++;
            }
        }
    }
}
