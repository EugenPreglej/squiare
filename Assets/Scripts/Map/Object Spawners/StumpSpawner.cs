using UnityEngine;

public class StumpSpawner : MonoBehaviour
{
    public GameObject stumpPrefab;
    private int numObj;

    public void Spawn()
    {
        numObj = Random.Range(7, 10);
        if (stumpPrefab == null) return;

        Bounds bounds = GetComponent<MeshFilter>().mesh.bounds;

        float minDistance = 20f;
        Vector3[] positions = new Vector3[numObj];
        int numSpawned = 0;

        while (numSpawned < numObj)
        {
            float x = Random.Range(bounds.min.x, bounds.max.x);
            float z = Random.Range(bounds.min.z, bounds.max.z);
            Vector3 position = new Vector3(x, 0, z);

            bool isTooClose = false;
            for (int i = 0; i < numSpawned; i++)
            {
                if (Vector3.Distance(position, positions[i]) < minDistance)
                {
                    isTooClose = true;
                    break;
                }
            }

            if (!isTooClose)
            {
                Quaternion rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
                GameObject spawned = Instantiate(stumpPrefab, transform.TransformPoint(position), rotation, transform);
                spawned.isStatic = true;
                positions[numSpawned] = position;
                numSpawned++;
            }
        }
    }
}
