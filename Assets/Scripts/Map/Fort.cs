using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Fort : MonoBehaviour
{
    private Collider collider;

    void Start()
    {
        collider = gameObject.GetComponent<Collider>();
    }

    void FixedUpdate()
    {
        SphereCollider sphereCollider = GetComponent<SphereCollider>();
        if (sphereCollider == null)
        {
            Debug.LogError("The sphere collider is not set!");
            return;
        }

        Collider[] colliders = Physics.OverlapSphere(transform.position + sphereCollider.center, sphereCollider.radius);

        foreach (Collider col in colliders)
        {
            if ((col.gameObject.name.StartsWith("Catapult") || col.gameObject.name.StartsWith("Tree") || col.gameObject.name.StartsWith("Stump") || col.gameObject.name.StartsWith("Shrub") || col.gameObject.name.StartsWith("Rocks")))
            {
                Destroy(col.gameObject);
            }
        }
    }

}
