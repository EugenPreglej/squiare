using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;
using Mapbox.Unity;
using Mapbox.Unity.Map;
using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;
using Mapbox.Utils.JsonConverters;
using Mapbox.Json;
using Mapbox.Directions;
using System.Linq;

public class DirectionsManager : MonoBehaviour
{
    public AbstractMap map;
    public Material pathMaterial;
    public Material savedPathMaterial;
    public GameObject playerTarget;
    public float proximityRadius = 3f;

    private string poiTarget;
    private Directions directions;
    private LineRenderer activeLineRenderer;
    private Vector3 activeEndPoint;
    private DirectionsResponse activeEndPointResponse;
    private POIManager poiManager;
    private UIManager uiManager;
    private SingleLocationProvider singleLocationProvider;

    private List<DirectionsResponse> directionsResponses = new List<DirectionsResponse>();
    private List<DirectionsResponse> tempDirectionsResponses = new List<DirectionsResponse>();
    private string directionsFilePath = "Responses.json";
    private string filePath;


    void Awake()
    {
        filePath = Application.persistentDataPath + "/" + directionsFilePath;

        singleLocationProvider = gameObject.GetComponent<SingleLocationProvider>();
        poiManager = gameObject.GetComponent<POIManager>();
        uiManager = gameObject.GetComponent<UIManager>();
    }


    private void Start()
    {
        directions = MapboxAccess.Instance.Directions;

        map.OnInitialized += () =>
        {
            ReadDirectionsResponses();
            foreach (DirectionsResponse dr in directionsResponses)
            {
                DrawOldRoute(dr);
            }
            directionsResponses = tempDirectionsResponses;
        };
    }


    private LineRenderer CreateNewLineRenderer(Material material, string name)
    {
        GameObject lineRendererObject = new GameObject(name);
        lineRendererObject.transform.SetParent(transform);
        LineRenderer lineRenderer = lineRendererObject.AddComponent<LineRenderer>();
        lineRenderer.material = material;
        lineRenderer.startWidth = 4f;
        lineRenderer.endWidth = 4f;
        lineRenderer.numCornerVertices = 5;
        lineRenderer.shadowCastingMode = ShadowCastingMode.Off;
        lineRenderer.generateLightingData = true;

        return lineRenderer;
    }


    private void ReadDirectionsResponses()
    {
        if (File.Exists(filePath))
        {
            string data = File.ReadAllText(filePath);
            directionsResponses = JsonConvert.DeserializeObject<List<DirectionsResponse>>(data);
        }
    }


    private void WriteDirectionsResponses()
    {
        var data = JsonConvert.SerializeObject(directionsResponses, Formatting.Indented, JsonConverters.Converters);
        File.WriteAllText(Application.persistentDataPath + "/" + directionsFilePath, data);
    }


    public void DrawRoute()
    {
        Dictionary<string, int> pointsOfInterest = poiManager.GetPOI();

        Vector2d currentLocation = singleLocationProvider.GetCurrentLocation();
        if (currentLocation != null)
        {
            //Debug.Log("Current location: " + currentLocation);
        }

        float minDistance = float.MaxValue;
        string closestPointOfInterest = null;

        foreach (KeyValuePair<string, int> kvp in pointsOfInterest)
        {
            if (kvp.Value == 0)
            {
                string[] coordinates = kvp.Key.Split(", ");
                Vector2d pointOfInterest = new Vector2d(float.Parse(coordinates[0]), float.Parse(coordinates[1]));

                float distance = (float) Vector2d.Distance(currentLocation, pointOfInterest);

                if (distance < minDistance)
                {
                    minDistance = distance;
                    closestPointOfInterest = kvp.Key;
                }
            }
        }

        poiTarget = closestPointOfInterest;

        //string end = "45.82330904210251, 15.969690724486615";
        Vector2d endLatLon = Conversions.StringToLatLon(closestPointOfInterest);
        activeEndPoint = map.GeoToWorldPosition(endLatLon, false);

        var directionResource = new DirectionResource(new[] { currentLocation, endLatLon }, RoutingProfile.Walking);
        directions.Query(directionResource, DrawNewRoute);
    }


    private void DrawOldRoute(DirectionsResponse response)
    {
        if (!tempDirectionsResponses.Contains(response))
        {
            tempDirectionsResponses.Add(response);
        }

        LineRenderer oldLineRenderer = CreateNewLineRenderer(savedPathMaterial, "Old LineRenderer");

        var points = response.Routes[0].Geometry;
        oldLineRenderer.positionCount = points.Count;

        for (int i = 0; i < points.Count; i++)
        {
            Vector2d pointLatLon = points[i];
            Vector3 pointWorldPosition = map.GeoToWorldPosition(pointLatLon, false);
            pointWorldPosition.y = 2f;
            oldLineRenderer.SetPosition(i, pointWorldPosition);
        }
        oldLineRenderer.Simplify(3);
    }


    private void DrawNewRoute(DirectionsResponse response)
    {
        if (response == null || response.Routes.Count < 1) return;

        activeEndPointResponse = response;
        activeLineRenderer = CreateNewLineRenderer(pathMaterial, "New Line Renderer");

        var points = response.Routes[0].Geometry;
        activeLineRenderer.positionCount = points.Count;

        float totalLength = 0f;
        for (int i = 0; i < activeLineRenderer.positionCount - 1; i++)
        {
            Vector3 currentPosition = activeLineRenderer.GetPosition(i);
            Vector3 nextPosition = activeLineRenderer.GetPosition(i + 1);

            totalLength += Vector3.Distance(currentPosition, nextPosition);
        }

        uiManager.StartCountdown((int) totalLength * 5);

        for (int i = 0; i < points.Count; i++)
        {
            Vector2d pointLatLon = points[i];
            Vector3 pointWorldPosition = map.GeoToWorldPosition(pointLatLon, false);
            pointWorldPosition.y = 2f;
            activeLineRenderer.SetPosition(i, pointWorldPosition);
        }
        activeLineRenderer.Simplify(3);
    }


    private void FixedUpdate()
    {
        if (activeLineRenderer != null && activeLineRenderer.positionCount > 1)
        {
            if (Vector3.Distance(playerTarget.transform.position, activeEndPoint) < proximityRadius)
            {
                poiManager.EditPOI(poiTarget, 1);
                activeLineRenderer.material = savedPathMaterial;
                
                ReadDirectionsResponses();
                if (!directionsResponses.Contains(activeEndPointResponse))
                {
                    directionsResponses.Add(activeEndPointResponse);
                    WriteDirectionsResponses();
                }
                //dodaj da onda moze novi request trazit
            }
        }
    }
}