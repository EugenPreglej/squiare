using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Map;
using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;

public class SingleLocationProvider : MonoBehaviour
{
    private ILocationProvider _locationProvider;
    private Vector2d currentLocation;


    void Start()
    {
        _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
    }


    public Vector2d GetCurrentLocation()
    {
        StartCoroutine(CurrentLocationCoroutine());
        StartCoroutine(WaitForCoroutine());
        return currentLocation;
    }


    private IEnumerator CurrentLocationCoroutine()
    {
        while (_locationProvider != null && _locationProvider.CurrentLocation.IsLocationServiceInitializing)
        {
            yield return null;
        }

        if (_locationProvider != null && _locationProvider.CurrentLocation.IsLocationServiceEnabled)
        {
            currentLocation = _locationProvider.CurrentLocation.LatitudeLongitude;
        }
        else
        {
            Debug.Log("Location services are not enabled.");
        }
        DestroyLocationProvider();
    }


    private IEnumerator WaitForCoroutine()
    {
        while (true)
        {
            yield return null;
            if (!CurrentLocationCoroutine().MoveNext())
            {
                break;
            }
        }
    }

    private void DestroyLocationProvider()
    {
        if (_locationProvider != null)
        {
            // Perform any required cleanup or disposal for your location provider here
            _locationProvider = null;
        }
    }
}