using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;

public class UIManager : MonoBehaviour
{
    public bool active;
    
    public GameObject welcomeWindow;
    public GameObject resultsWindow;
    public Button startButton;
    public Text timer;

    [HideInInspector]
    public string selectedItem;

    private DateTime targetTime;
    private bool isCountingDown = false;
    

    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Map")
        {
            timer.transform.parent.gameObject.SetActive(false);
            HideResultsWindow();
        }
    }

    
    public void UIActive(bool isActive)
    {
        active = isActive;
    }


    private string FormatTimeSpan(TimeSpan timeSpan)
    {
        return string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
    }


    public void HideWelcomeWindow()
    {
        welcomeWindow.SetActive(false);
        startButton.gameObject.SetActive(true);
        UIActive(false);
    }


    public void ShowWelcomeWindow()
    {
        welcomeWindow.SetActive(true);
        startButton.gameObject.SetActive(false);
        UIActive(true);
    }


    public void HideResultsWindow()
    {
        resultsWindow.SetActive(false);
        startButton.gameObject.SetActive(true);
        UIActive(false);
    }


    public void ShowResultsWindow(bool completed)
    {
        if(completed) 
        {
            resultsWindow.transform.Find("Window/Content/Text").gameObject.GetComponent<Text>().text = "GG";
        }
        else
        {
            resultsWindow.transform.Find("Window/Content/Text").gameObject.GetComponent<Text>().text = "u succ";
        }

        resultsWindow.SetActive(true);
        startButton.gameObject.SetActive(false);
        UIActive(true);
    }


    public void HideStartButton()
    {
        startButton.gameObject.SetActive(false);
    }


    public void ShowStartButton()
    {
        startButton.gameObject.SetActive(true);
    }


    public void StartCountdown(int seconds)
    {
        timer.transform.parent.gameObject.SetActive(true);
        timer.text = FormatTimeSpan(TimeSpan.FromSeconds(seconds));
        targetTime = DateTime.Now.AddSeconds(seconds);

        isCountingDown = true;
    }


    void Update()
    {
        if (!isCountingDown)
        {
            return;
        }

        TimeSpan timeRemaining = targetTime - DateTime.Now;

        if (timeRemaining.TotalSeconds <= 0)
        {
            isCountingDown = false;
            timer.text = "Time's up!";
            ShowResultsWindow(false);
        }
        else
        {
            timer.text = FormatTimeSpan(timeRemaining);
        }
    }


    public void SelectItem(GameObject button)
    {
        selectedItem = button.name;

        Outline[] outlines = button.transform.parent.gameObject.GetComponentsInChildren<Outline>();
        foreach (Outline outline in outlines)
        {
            outline.enabled = false;
        }

        button.GetComponent<Outline>().enabled = true;
    }
}