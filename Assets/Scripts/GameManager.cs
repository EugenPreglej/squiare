using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private UIManager uiManager;

    void Awake()
    {
        uiManager = gameObject.GetComponent<UIManager>();
    }

    void Start()
    {
        if (!PlayerPrefs.HasKey("welcomeWindowShown"))
        {
            uiManager.ShowWelcomeWindow();
            PlayerPrefs.SetInt("welcomeWindowShown", 1);
        }
        else
        {
            uiManager.HideWelcomeWindow();
        }
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
