using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class ClickManager : MonoBehaviour
{
    private float maxDistance = 200000f;
    private GameObject playerTarget;
    private InventoryManager inventoryManager;
    private UIManager uiManager;


    void Start()
    {
        playerTarget = GameObject.Find("PlayerTarget");
        uiManager = GameObject.Find("Manager").GetComponent<UIManager>();
        inventoryManager = GameObject.Find("Manager").GetComponent<InventoryManager>();
    }


    void Update()
    {
        if (!uiManager.active && Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.CompareTag("Fort"))
                {
                    float distanceToPlayer = Vector3.Distance(transform.position, playerTarget.transform.position);
                    if (distanceToPlayer <= maxDistance)
                    {
                        SceneManager.LoadScene("AR");
                    }
                }

                else if (hit.collider.CompareTag("Catapult"))
                {
                    float distanceToPlayer = Vector3.Distance(transform.position, playerTarget.transform.position);
                    if (distanceToPlayer <= maxDistance)
                    {
                        inventoryManager.AddItem("Catapult");
                        Destroy(hit.transform.gameObject);
                    }
                }

                else
                {
                    //Debug.Log(hit.transform.gameObject.name);
                }
            }
        }

        else if (uiManager.active && Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit, Mathf.Infinity);
        }
    }
}
