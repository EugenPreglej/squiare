using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARPlacer : MonoBehaviour
{
    public GameObject fortPrefab;
    public GameObject catapultPrefab;
    public GameObject projectilePrefab;
    public Vector3 fortLocation;

    private ARRaycastManager arRaycastManager;
    private List<ARRaycastHit> hits = new List<ARRaycastHit>();
    private InventoryManager inventoryManager;
    private UIManager uiManager;

    private bool fortPlaced = false;
    private float lastPlacementTime;
    private float placementCooldown = 0.5f;
    private Dictionary<string, int> inventory;


    void Awake()
    {
        arRaycastManager = gameObject.GetComponent<ARRaycastManager>();
    }


    void Start()
    {
        inventoryManager = GameObject.Find("Manager").GetComponent<InventoryManager>();
        uiManager = GameObject.Find("Manager").GetComponent<UIManager>();
    }


    void Update()
    {
        if (!fortPlaced)
        {
            PlaceFort();
        }
        else
        {
            PlaceAttackers();
        }
    }


    void PlaceFort()
    {
        if (Input.touchCount > 0)
        {
            if (arRaycastManager.Raycast(Input.GetTouch(0).position, hits, TrackableType.PlaneWithinPolygon))
            {
                var hitPose = hits[0].pose;

                GameObject spawnedPrefab = Instantiate(fortPrefab, hitPose.position, hitPose.rotation);
                //Instantiate(projectilePrefab, hitPose.position + Vector3.up * 2f, hitPose.rotation);
                fortPlaced = true;
                //fortLocation = hitPose.position;
                lastPlacementTime = Time.time;
            }
        }
    }


    void PlaceAttackers()
    {
        if (Input.touchCount > 0 && Time.time - lastPlacementTime > placementCooldown)
        {
            if (arRaycastManager.Raycast(Input.GetTouch(0).position, hits))
            {
                var hitPose = hits[0].pose;

                if (uiManager.selectedItem == "Catapult")
                {
                    inventory = inventoryManager.GetInventory();

                    if (inventory.ContainsKey("Catapult"))
                    {
                        Collider[] colliders = Physics.OverlapSphere(hitPose.position, 0.15f);

                        bool canSpawn = true;

                        foreach (Collider collider in colliders)
                        {
                            if (collider.transform.parent != null && collider.transform.parent.CompareTag("Catapult"))
                            {
                                canSpawn = false;
                                break;
                            }
                        }

                        if (canSpawn)
                        {
                            inventoryManager.RemoveItem("Catapult");
                            inventory = inventoryManager.GetInventory();

                            GameObject spawnedPrefab = Instantiate(catapultPrefab, hitPose.position, hitPose.rotation);
                            lastPlacementTime = Time.time;
                            Debug.Log(inventory["Catapult"]);
                        }

                        else
                        {
                            Debug.Log("Can't place a new catapult, there is already one in the range");
                        }
                    }
                }
            }
        }
    }


}
