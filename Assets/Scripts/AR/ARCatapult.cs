using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARCatapult : MonoBehaviour
{
    public GameObject projectilePrefab;
    private float shootForce = 400f;
    private float shootInterval = 4f;

    private float timer = 0f;

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= shootInterval)
        {
            Shoot();
            timer = 0f;
        }
    }

    private void Shoot()
    {
        GameObject projectile = Instantiate(projectilePrefab, transform.position + Vector3.up * 0.09f, Quaternion.identity);
        Rigidbody rb = projectile.GetComponent<Rigidbody>();

        if (rb != null)
        {
            Vector3 launchDirection = (transform.forward + transform.up).normalized;
            rb.AddForce(launchDirection * shootForce, ForceMode.Impulse);
        }
        else
        {
            Debug.LogError("Projectile prefab doesn't have a Rigidbody component.");
        }
    }


}
