using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARProjectile : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Fort"))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        //Destroy(gameObject);
    }
}
