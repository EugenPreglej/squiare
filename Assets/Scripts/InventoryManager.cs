using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;
using Mapbox.Unity;
using Mapbox.Unity.Map;
using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;
using Mapbox.Utils.JsonConverters;
using Mapbox.Json;
using Mapbox.Directions;

public class InventoryManager : MonoBehaviour
{
    private int amount;
    private Dictionary<string, int> inventory = new Dictionary<string, int>();
    private string inventoryFilePath = "Inventory.json";
    private string filePath;

    
    void Awake()
    {
        filePath = Application.persistentDataPath + "/" + inventoryFilePath;
    }


    void Start()
    {
        GetInventory();
    }


    private void ReadInventory()
    {
        if (File.Exists(filePath))
        {
            string data = File.ReadAllText(filePath);
            inventory = JsonConvert.DeserializeObject<Dictionary<string, int>>(data);
        }
    }


    private void WriteInventory()
    {
        var data = JsonConvert.SerializeObject(inventory, Formatting.Indented, JsonConverters.Converters);
        File.WriteAllText(Application.persistentDataPath + "/" + inventoryFilePath, data);
    }


    public Dictionary<string, int> GetInventory()
    {
        string filePath = Application.persistentDataPath + "/" + inventoryFilePath;
        Debug.Log(filePath);

        if (File.Exists(filePath))
        {
            ReadInventory();
        }
        else
        {
            WriteInventory();
        }

        return inventory;
    }


    public void AddItem(string objectName)
    {
        GetInventory();

        if (!inventory.ContainsKey(objectName))
        {
            inventory.Add(objectName, 1);
        }
        
        else
        {
            amount = inventory[objectName];
            amount ++;
            inventory[objectName] = amount;
        }
        
        WriteInventory();
    }


    public void RemoveItem(string objectName)
    {
        GetInventory();

        if (inventory.ContainsKey(objectName))
        {
            if (inventory[objectName] != 1)
            {
                amount = inventory[objectName];
                amount--;
                inventory[objectName] = amount;
            }

            else
            {
                inventory.Remove(objectName);
            }

            WriteInventory();
        }
    }
}